package com.pym.pcmanagementapi.controller;

import com.pym.pcmanagementapi.model.PcManagementItem;
import com.pym.pcmanagementapi.model.PcManagementRequest;
import com.pym.pcmanagementapi.model.PcManagementResponse;
import com.pym.pcmanagementapi.service.PcManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/management")
public class PcManagementController {
    private final PcManagementService pcManagementService;

    @PostMapping("/pc")
    public String setPcManagement(@RequestBody PcManagementRequest request){
        pcManagementService.setPcManagement(request);

        return "점검완료";
    }

    @GetMapping("/all")
    public List <PcManagementItem> getManagements(){
        return pcManagementService.getManagements();
    }

    @GetMapping("/detail/{id}")
    public PcManagementResponse getManagement(@PathVariable long id){
        return pcManagementService.getManagement(id);
    }
}
