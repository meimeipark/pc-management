package com.pym.pcmanagementapi.service;

import com.pym.pcmanagementapi.entity.PcManagement;
import com.pym.pcmanagementapi.model.PcManagementItem;
import com.pym.pcmanagementapi.model.PcManagementRequest;
import com.pym.pcmanagementapi.model.PcManagementResponse;
import com.pym.pcmanagementapi.repository.PcManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcManagementService {
    private final PcManagementRepository pcManagementRepository;

    public void setPcManagement (PcManagementRequest request){
        PcManagement addData = new PcManagement();
        addData.setSeatName(request.getSeatName());
        addData.setDateCreate(LocalDate.now());
        addData.setPcCheck(request.getPcCheck());
        addData.setCheckMemo(request.getCheckMemo());
        addData.setIsUpgrade(request.getIsUpgrade());

        pcManagementRepository.save(addData);
    }

    public List<PcManagementItem> getManagements(){
        List<PcManagement> originList = pcManagementRepository.findAll();

        List<PcManagementItem> result = new LinkedList<>();

        for(PcManagement pcManagement: originList){
            PcManagementItem addItem = new PcManagementItem();
            addItem.setId(pcManagement.getId());
            addItem.setSeatName(pcManagement.getSeatName());
            addItem.setDateCreate(pcManagement.getDateCreate());
            addItem.setPcCheck(pcManagement.getPcCheck().getName());
            addItem.setIsChange(pcManagement.getPcCheck().getIsChange());
            addItem.setCheckMemo(pcManagement.getCheckMemo());
            addItem.setIsUpgrade(pcManagement.getIsUpgrade());

            result.add(addItem);
        }
        return result;
    }

    public PcManagementResponse getManagement(long id) {
        PcManagement originData = pcManagementRepository.findById(id).orElseThrow();

        PcManagementResponse response = new PcManagementResponse();
        response.setId(originData.getId());
        response.setSeatName(originData.getSeatName());
        response.setDateCreate(originData.getDateCreate());
        response.setPcCheckName(originData.getPcCheck().getName());
        response.setIsChange(originData.getPcCheck().getIsChange() ? "교체 필요":"교체 불필요");
        response.setCheckMemo(originData.getCheckMemo());
        response.setIsUpgrade(originData.getIsUpgrade() ? "업그레이드 필요":"업그레이드 불필요");

        return response;
    }
}
