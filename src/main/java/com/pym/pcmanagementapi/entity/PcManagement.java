package com.pym.pcmanagementapi.entity;

import com.pym.pcmanagementapi.enums.PcCheck;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PcManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Short seatName;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PcCheck pcCheck;

    @Column(columnDefinition = "TEXT")
    private String checkMemo;

    @Column(nullable = false)
    private Boolean isUpgrade;
}
