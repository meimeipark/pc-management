package com.pym.pcmanagementapi.model;

import com.pym.pcmanagementapi.enums.PcCheck;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Getter
@Setter
public class PcManagementRequest {
    private Short seatName;
    @Enumerated(value = EnumType.STRING)
    private PcCheck pcCheck;
    private String checkMemo;
    private Boolean isUpgrade;
}
