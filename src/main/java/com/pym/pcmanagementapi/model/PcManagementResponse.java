package com.pym.pcmanagementapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementResponse {
    private Long id;
    private Short seatName;
    private LocalDate dateCreate;
    private String pcCheckName;
    private String isChange;
    private String checkMemo;
    private String isUpgrade;

}
