package com.pym.pcmanagementapi.model;

import com.pym.pcmanagementapi.enums.PcCheck;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementItem {
    private Long id;
    private Short seatName;
    private LocalDate dateCreate;
    private String pcCheck;
    private Boolean isChange;
    private String checkMemo;
    private Boolean isUpgrade;
}
