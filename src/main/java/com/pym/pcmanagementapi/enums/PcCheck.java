package com.pym.pcmanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcCheck {
    NORMAL("정상", false),
    FAULTY("불량", true);

    private final String name;
    private final Boolean isChange;
}
