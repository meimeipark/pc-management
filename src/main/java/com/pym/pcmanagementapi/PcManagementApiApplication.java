package com.pym.pcmanagementapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcManagementApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcManagementApiApplication.class, args);
    }

}
