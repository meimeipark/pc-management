package com.pym.pcmanagementapi.repository;

import com.pym.pcmanagementapi.entity.PcManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementRepository extends JpaRepository <PcManagement, Long> {
}
